package io.github.vladimirmi.photon.data.models

/**
 * Created by Vladimir Mikhalev 01.06.2017.
 */

data class LoginDto(var username: String = "", var password: String = "")