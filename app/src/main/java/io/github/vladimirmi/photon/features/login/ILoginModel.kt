package io.github.vladimirmi.photon.features.login

import io.github.vladimirmi.photon.core.IModel

/**
 * Developer Vladimir Mikhalev 30.05.2017
 */

interface ILoginModel : IModel
