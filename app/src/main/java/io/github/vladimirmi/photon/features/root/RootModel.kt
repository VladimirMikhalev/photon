package io.github.vladimirmi.photon.features.root

import io.github.vladimirmi.photon.data.managers.DataManager

/**
 * Developer Vladimir Mikhalev 30.05.2017
 */

class RootModel(private val mDataManager: DataManager) : IRootModel
