package io.github.vladimirmi.photon.features.login

import io.github.vladimirmi.photon.data.managers.DataManager

/**
 * Developer Vladimir Mikhalev 30.05.2017
 */

class LoginModel(private val mDataManager: DataManager) : ILoginModel
