package io.github.vladimirmi.photon.features.root

import io.github.vladimirmi.photon.core.IModel

/**
 * Developer Vladimir Mikhalev 30.05.2017
 */

interface IRootModel : IModel
