package io.github.vladimirmi.photon.features.login

import io.github.vladimirmi.photon.core.IView

/**
 * Developer Vladimir Mikhalev 30.05.2017
 */

interface ILoginView : IView
